<?php
/**
 * @file
 * gk_rewards_signup.features.inc
 */

/**
 * Implements hook_default_reward_type().
 */
function gk_rewards_signup_default_reward_type() {
  $items = array();
  $items['signup_reward'] = entity_import('reward_type', '{
    "type" : "signup_reward",
    "label" : "Sign-up",
    "description" : ""
  }');
  return $items;
}
