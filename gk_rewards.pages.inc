<?php

/**
 * Page callback for /reward/%reward.
 */
function reward_view_page($reward) {
  global $user;

  // Check this reward is viewable on the front-end (public) and that it is
  // published (status).
  if ($reward->public && ($reward->status || $user->uid == 1)) {
    return reward_view($reward);
  }

  return MENU_NOT_FOUND;
}

/**
 * Page callback for /confirm-my-details.
 */
function gk_rewards_confirm_my_details_page_callback() {
  // Check we have everything we need to confirm the user's details.
  if (
         empty($_GET['h'])
      || !(($user_details = gk_rewards_user_details_load($_GET['h'], 'hash')) && $user_details = $user_details[0])
      || !(($fulfilment = gk_rewards_fulfilment_load($user_details->rfid)) && $fulfilment = $fulfilment[0])
  ) {
    drupal_goto();
  }

  // Let everyone know that the details are being verified.
  $user_details->verifying = TRUE;

  // Update the status of the fulfilment record and if there's a Drupal user
  // account that matches these user details then also update it's ownership.
  $fulfilment->status = 1;

  if ($account = user_load_by_mail($user_details->mail)) {
    $fulfilment->uid = $account->uid;
  }

  $fulfilment->changed = REQUEST_TIME;
  drupal_write_record('gk_rewards_fulfilments', $fulfilment, 'rfid');

  // Invoke the fulfilment hooks in order to fulfil the reward.
  $reward = reward_load($fulfilment->rid);
  gk_rewards_fulfil_invoke_hooks($reward, $fulfilment, $user_details);

  // Invoke the 'user_details_confirm' hook.
  module_invoke_all('gk_rewards_user_details_confirm', $user_details, $fulfilment, $reward);

  // If a redirect path has been set for the confirm details step of this reward
  // type then redirect there.
  if ($path = variable_get('gk_rewards_confirm_details_redirect_path_' . $reward->type)) {
    drupal_goto($path, array(
      'query' => array(
        'h' => $user_details->hash,
      ),
    ));
  }

  // ... otherwise set a message and go home.
  drupal_set_message(t('Thank you for confirming your details.'));
  drupal_goto();
}

/**
 * Page callback for making the fulfilment notification hooks for a reward
 * invocable over HTTP (reward/%reward/fulfil/%user/notify).
 */
function gk_rewards_fulfil_notify_page_callback($reward, $account) {
  $query = db_select('gk_rewards_fulfilments', 'rf')->fields('rf')
    ->condition('uid', $account->uid)->condition('rid', $reward->rid);

  // The access callback for this path already checked that the fulfilment
  // record exists, but it's still possible for the super user to get here.
  if (!$fulfilment = $query->execute()->fetchObject()) {
    drupal_access_denied();
    drupal_exit();
  }

  $user_details = gk_rewards_user_details_from_account($account);

  // Debug mode available to the super user.
  if ($GLOBALS['user']->uid == 1 && isset($_GET['debug']) && module_exists('devel')) {
    dpm($fulfilment, "fulfilment");
    dpm($user_details, "user details");

    return "Debug mode. No hooks invoked.";
  }

  // Invoke the notification hooks.
  module_invoke_all('gk_rewards_' . $reward->type . '_notify', $reward, $fulfilment, $user_details);

  // Set a message and redirect back to the reward full page view.
  drupal_set_message(t('The reward has been sent.'));
  drupal_goto('reward/' . $reward->rid);
}
