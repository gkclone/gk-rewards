<div class="reward-tier-notify">
  <table class="row prose reward-tier-notify__intro">
    <tr>
      <td>
        <?php echo $intro ?>
      </td>
    </tr>
  </table>

  <table class="row prose reward-tier-notify__summary">
    <tr>
      <td>
        <?php echo $summary ?>
      </td>
    </tr>
  </table>

  <?php if (!empty($ctas)): ?>
    <?php foreach ($ctas as $cta): ?>
      <table class="row prose reward-tier-notify__cta">
        <tr>
          <td class="center">

            <table class="medium-button">
              <tr>
                <td>
                  <a href="<?php echo $cta['url'] ?>"><?php echo $cta['title'] ?></a>
                </td>
              </tr>
            </table>

          </td>
        </tr>
      </table>
    <?php endforeach; ?>
  <?php endif; ?>

  <table class="row prose reward-tier-notify__outro">
    <tr>
      <td>
        <?php echo $outro ?>
      </td>
    </tr>
  </table>
</div>
