<?php

/**
 * @file
 * Entity controller classes for the reward(_type) entities.
 */

/**
 * Entity controller class: reward
 */
class RewardController extends EntityAPIController {
  public function create(array $values = array()) {
    global $user;

    $values += array(
      'title' => '',
      'public' => 1,
      'status' => 1,
      'uid' => $user->uid,
      'fulfilment_code' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );

    return parent::create($values);
  }
}
