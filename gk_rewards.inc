<?php

/**
 * Function for fulfilling a reward.
 *
 * @param $reward Object
 *   A reward entity object.
 *
 * @param $user_details Array
 *   An array containing either:
 *     - a Drupal user ID in the 'uid' key indicating that the fulfilment is for
 *       an authenticated user, or
 *
 *     - the following mandatory keys indicating that the fulfilment is for an
 *       anonymous user:
 *         - mail
 *         - title
 *         - first_name
 *         - last_name
 *         - postcode
 *         - date_of_birth
 *         - comms_opt_in_brand
 *         - comms_opt_in_other
 *
 * @param $notify Boolean
 *   Defaults to TRUE. Determines whether or not to invoke the notify hook (only
 *   applies if the fulfilment record status is 1, since a status of 0 defers
 *   invocation of the hooks until the anonymous user confirms their details).
 */
function gk_rewards_fulfil($reward, $user_details, $notify = TRUE) {
  // Load the reward if a rid was given.
  if (is_numeric($reward)) {
    $reward = reward_load($reward);
  }

  // Assume we don't have a logged in user and therefore that the fulfilment has
  // an initial status of 0.
  $uid = NULL;
  $status = 0;

  if (isset($user_details['uid'])) {
    $uid = $user_details['uid'];
    $status = 1;
  }

  // Generate a code for this fulfilment.
  if (!$code = $reward->fulfilment_code) {
    $code = '[current-date:raw]-[random:uniqid]';
  }

  $code = token_replace($code);

  // Record this fulfilment in the database.
  $fulfilment = (object) array(
    'rid' => $reward->rid,
    'uid' => $uid,
    'code' => $code,
    'status' => $status,
    'created' => REQUEST_TIME,
    'changed' => REQUEST_TIME,
  );
  drupal_write_record('gk_rewards_fulfilments', $fulfilment);

  // If the fulfilment has a status of 1 then invoke the fulfilment hooks.
  if ($fulfilment->status == 1) {
    $user_details_from_account = gk_rewards_user_details_from_account(user_load($uid));

    // Make sure that the user details object we pass to the hooks contains any
    // additional info that might be in the $user_details data array, which may
    // have been modified during form submission.
    if (!empty($user_details['data'])) {
      $user_data = &$user_details_from_account->data;
      $user_data = gk_core_options_extend($user_data, $user_details['data']);
    }

    gk_rewards_fulfil_invoke_hooks($reward, $fulfilment, $user_details_from_account, $notify);
  }
  // Otherwise, the user was anonymous and we require that they confirm their
  // details before we invoke the hooks. Save their details in the user_details
  // table and send them an email asking for confirmation. The fulfilment hooks
  // will be invoked later during a page load clicked through from the email.
  else {
    $hash = gk_core_hash($fulfilment->rfid . REQUEST_TIME);

    $user_details += array(
      'hash' => $hash,
      'rfid' => $fulfilment->rfid,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );
    drupal_write_record('gk_rewards_user_details', $user_details);

    $params = array(
      'user_details' => (object) $user_details,
      'fulfilment' => $fulfilment,
    );
    drupal_mail('gk_rewards', 'confirm_details', $user_details['mail'], language_default(), $params);
  }

  return $fulfilment;
}

/**
 * Function for invoking the hooks associated with fulfilling a reward. These
 * are in a separate function because they need to be called in two places:
 *   - in gk_rewards_fulfil() if the user is logged in, and
 *   - in the confirm details page callback if the user is anonymous
 */
function gk_rewards_fulfil_invoke_hooks($reward, $fulfilment, $user_details, $notify = TRUE) {
  // Invoke the general fulfilment hook. Modules should implement this in order
  // to perform tasks such as updating database records.
  module_invoke_all('gk_rewards_fulfilment', $reward, $fulfilment, $user_details);

  // Invoke the hook specific to this type of reward.
  module_invoke_all('gk_rewards_' . $reward->type . '_fulfilment', $reward, $fulfilment, $user_details);

  // Additionally, modules should implement the notify hook in order to let the
  // user (and potentially anyone else involved) know that the reward has been
  // fulfilled.
  if ($notify) {
    module_invoke_all('gk_rewards_' . $reward->type . '_notify', $reward, $fulfilment, $user_details);
  }
}

/**
 * Function for loading a record from the gk_rewards_fulfilments table.
 */
function gk_rewards_fulfilment_load($value, $field = 'rfid') {
  $query = db_select('gk_rewards_fulfilments', 'f')
    ->fields('f')->condition($field, $value)->orderBy('created');

  return $query->execute()->fetchAll();
}

/**
 * Function for loading a record from the gk_rewards_user_details table.
 */
function gk_rewards_user_details_load($value, $field = 'udid') {
  $query = db_select('gk_rewards_user_details', 'ud')
    ->fields('ud')->condition($field, $value);

  if ($user_details = $query->execute()->fetchAll()) {
    // Unserialize the 'data' field.
    foreach ($user_details as $record) {
      $record->data = unserialize($record->data);
    }
  }

  return $user_details;
}

/**
 * Function for creating a user details object given a Drupal user account.
 */
function gk_rewards_user_details_from_account($account) {
  $user_details = array(
    'uid' => $account->uid,
    'mail' => $account->mail,
    'data' => $account->data,
    'created' => $account->created,
  );

  $user_profile_fields = array(
    'title', 'first_name', 'last_name', 'postcode', 'date_of_birth', 'comms_opt_in_brand', 'comms_opt_in_other',
  );

  foreach ($user_profile_fields as $field) {
    $value = NULL;

    if ($field_value = field_get_items('user', $account, 'field_user_' . $field)) {
      $value = $field_value[0]['value'];
    }

    $user_details[$field] = $value;
  }

  // Field API stores dates as a string with hours/minutes/seconds on the end,
  // even when told not to collect that data. Everywhere we use this field we
  // want it to be in the same format, so we'll trim it down here.
  if ($user_details['date_of_birth']) {
    $user_details['date_of_birth'] = substr($user_details['date_of_birth'], 0, 10);
  }

  return (object) $user_details;
}

/**
 * Function for creating a Drupal user account given a user details record.
 */
function gk_rewards_create_account($user_details, $password = NULL) {
  if (empty($password)) {
    $password = user_password();
  }

  $username = gk_rewards_username_from_mail($user_details->mail);

  $fields = array(
    'name' => $username,
    'mail' => $user_details->mail,
    'init' => $user_details->mail,
    'pass' => $password,
    'status' => 1,
    'roles' => array(
      DRUPAL_AUTHENTICATED_RID => 'authenticated user',
    ),
  );

  $user_profile_fields = array(
    'title', 'first_name', 'last_name', 'postcode', 'date_of_birth', 'comms_opt_in_brand', 'comms_opt_in_other',
  );

  foreach ($user_profile_fields as $field) {
    if (!empty($user_details->$field)) {
      $fields['field_user_' . $field][LANGUAGE_NONE][0]['value'] = $user_details->$field;
    }
  }

  // Allow modules to alter the account fields before saving.
  drupal_alter('gk_rewards_create_account_fields', $fields, $user_details);

  // Create the new account.
  $account = user_save(NULL, $fields);

  // Invoke the 'account_created' hook.
  module_invoke_all('gk_rewards_account_created', user_load($account->uid), $user_details);

  return $account;
}

/**
 * Function for updating a Drupal user account given an array of values. This
 * should look similar to a user details object. It is only possible to update
 * the following user fields with this function:
 *   - mail
 *   - title
 *   - first_name
 *   - last_name
 *   - postcode
 *   - date_of_birth
 */
function gk_rewards_update_account($account, array $user_details) {
  // Load the account if a user ID was given.
  if (is_numeric($account)) {
    $account = user_load($account);
  }

  $edit = array(
    'mail' => $user_details['mail'],
  );

  foreach (array('title', 'first_name', 'last_name', 'postcode', 'date_of_birth') as $field) {
    if (!empty($user_details[$field])) {
      $edit['field_user_' . $field][LANGUAGE_NONE][0]['value'] = $user_details[$field];
    }
  }

  // Allow modules to alter the account fields before saving.
  drupal_alter('gk_rewards_update_account_fields', $edit, $user_details, $account);

  // Update the account.
  $account = user_save($account, $edit);

  // Invoke the 'account_updated' hook.
  module_invoke_all('gk_rewards_account_updated', $account, $user_details);

  return $account;
}

/**
 * Function to update the 'uid' field of all fulfilment records that belong to
 * the mail address found in the given user details record.
 */
function gk_rewards_update_fulfilment_ownership($uid, $udid) {
  if (!$user_details = gk_rewards_user_details_load($udid)) {
    return FALSE;
  }

  $user_details = $user_details[0];

  $query = db_select('gk_rewards_user_details', 'ud')
    ->fields('ud', array('rfid'))
    ->condition('mail', $user_details->mail);

  $rfids = $query->execute()->fetchAllAssoc('rfid');
  $rfids = array_keys($rfids);

  return db_update('gk_rewards_fulfilments')
    ->fields(array(
      'uid' => $uid,
      'changed' => REQUEST_TIME,
    ))
    ->condition('rfid', $rfids)
    ->execute();
}

/**
 * Function for generating a unique username given an email address.
 */
function gk_rewards_username_from_mail($mail) {
  // Strip off everything after the @ sign.
  $username = preg_replace('/@.*$/', '', $mail);

  // Strip illegal characters.
  $username = preg_replace('/[^\x{80}-\x{F7} a-zA-Z0-9@_.\'-]/', '', $username);

  // Handle spaces.
  $username = preg_replace('/ +/', '_', trim($username));

  // If there's nothing left use a default.
  if (!$username) {
    $username = t('user');
  }

  // Truncate to a reasonable size.
  if (drupal_strlen($username) > (USERNAME_MAX_LENGTH - 10)) {
    $username = drupal_substr($username, 0, USERNAME_MAX_LENGTH - 11);
  }

  // Iterate until we find a unique username.
  $i = 0;

  do {
    $new_username = $i ? $username . '_' . $i : $username;
    $query = db_select('users')->condition('name', $new_username)->countQuery();
  } while ($query->execute()->fetchField() && ++$i);

  return $new_username;
}

/**
 * Function for determining the number of successful referrals made by the user
 * in the context of the given reward tier group.
 */
function gk_rewards_tier_group_user_referral_count($tier_group, $uid = NULL) {
  $referral_count = 0;

  if (!empty($tier_group)) {
    // Calculate the number of referrals the user has in this tier group.
    static $tier_group_referral_counts = array();

    if (!isset($tier_group_referral_counts[$tier_group->tgid])) {
      // Assume no referrals have been made for this group.
      $tier_group_referral_counts[$tier_group->tgid] = 0;

      // Get the users referrals.
      static $user_referrals = NULL;
      if (!isset($user_referrals)) {
        if (!isset($uid)) {
          global $user;
          $uid = $user->uid;
        }

        $user_referrals = gk_referrals_user_referrals($uid);

        // Filter out referrals that have not been confirmed.
        foreach ($user_referrals as $key => $referral) {
          if (!$referral->status) {
            unset($user_referrals[$key]);
          }
        }
      }

      if ($user_referrals) {
        if ($tier_group_start_date_field = field_get_items('tier_group', $tier_group, 'field_tier_group_dates')) {
          $tier_group_start_date = strtotime($tier_group_start_date_field[0]['value']);

          foreach ($user_referrals as $referral) {
            if ($referral->created >= $tier_group_start_date) {
              $tier_group_referral_counts[$tier_group->tgid] += 1;
            }
          }
        }
        else {
          $tier_group_referral_counts[$tier_group->tgid] = count($user_referrals);
        }
      }
    }

    $referral_count = $tier_group_referral_counts[$tier_group->tgid];
  }

  return $referral_count;
}
