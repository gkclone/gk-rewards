<?php
/**
 * @file
 * gk_rewards.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gk_rewards_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_tier_group_type().
 */
function gk_rewards_default_tier_group_type() {
  $items = array();
  $items['reward_tier_group'] = entity_import('tier_group_type', '{
    "type" : "reward_tier_group",
    "label" : "Reward tier group",
    "description" : ""
  }');
  return $items;
}

/**
 * Implements hook_default_tier_type().
 */
function gk_rewards_default_tier_type() {
  $items = array();
  $items['reward_tier'] = entity_import('tier_type', '{ "type" : "reward_tier", "label" : "Reward tier", "description" : "" }');
  return $items;
}
