<?php

/**
 * @file
 * Entity classes for the reward(_type) entities.
 */

/**
 * Entity class: reward
 */
class Reward extends Entity {
  protected function defaultUri() {
    return array(
      'path' => 'reward/' . $this->identifier(),
    );
  }

  public function buildContent($view_mode = 'full', $langcode = NULL) {
    $build = parent::buildContent($view_mode, $langcode);

    // Add the sign-up form in full view mode.
    if ($view_mode == 'full') {
      $user_details = NULL;
      global $user;

      if ($user->uid && variable_get('gk_rewards_prepopulate_user_details_form_' . $this->type, 1)) {
        $user_details = gk_rewards_user_details_from_account(user_load($user->uid));
      }

      $build['user_details_form'] = drupal_get_form('gk_rewards_user_details_form', $user_details, $this);
      $build['user_details_form']['#weight'] = +10;
    }

    return $build;
  }
}
