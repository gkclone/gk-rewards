<?php

/**
 * @file
 * Admin UI for the reward(_type) entities.
 */

/**
 * Entity: reward ==============================================================
 */

/**
 * Entity form: reward
 */
function reward_form($form, &$form_state, $reward) {
  $form_state['reward'] = $reward;
  $entity_id = entity_id('reward', $reward);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => 'Name',
    '#required' => TRUE,
    '#default_value' => $reward->title,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $reward->uid,
  );

  // Field API form elements.
  field_attach_form('reward', $reward, $form, $form_state);

  // Fulfilment.
  $form['fulfilment_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Fulfilment options',
    '#group' => 'additional_settings',
  );

  $form['fulfilment_options']['fulfilment_code'] = array(
    '#type' => 'textfield',
    '#title' => 'Code',
    '#default_value' => $reward->fulfilment_code,
  );

  // Publishing options.
  $form['publishing_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Publishing options',
    '#group' => 'additional_settings',
  );

  $form['publishing_options']['status'] = array(
    '#type' => 'checkbox',
    '#title' => 'Published',
    '#default_value' => $reward->status,
  );

  $form['publishing_options']['public'] = array(
    '#type' => 'checkbox',
    '#title' => 'Public',
    '#default_value' => $reward->public,
  );

  // Vertical tabs.
  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 900,
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );

  return $form;
}

/**
 * Submit handler for the reward entity form.
 */
function reward_form_submit($form, &$form_state) {
  $reward = $form_state['reward'];

  entity_form_submit_build_entity('reward', $reward, $form, $form_state);
  entity_save('reward', $reward);

  $uri = entity_uri('reward', $reward);
  $form_state['redirect'] = $uri['path'];

  drupal_set_message(t('Reward %title saved.', array(
    '%title' => entity_label('reward', $reward),
  )));
}

/**
 * Entity: reward_type =========================================================
 */

/**
 * Entity form: reward_type
 */
function reward_type_form($form, &$form_state, $reward_type, $op = 'edit') {
  if ($op == 'clone') {
    $reward_type->label .= ' (cloned)';
    $reward_type->type = '';
  }

  $form_state['reward_type'] = $reward_type;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($reward_type->label) ? $reward_type->label : '',
    '#description' => t('The human-readable name of this type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($reward_type->type) ? $reward_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $reward_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'gk_rewards_type_list',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($reward_type->description) ? $reward_type->description : '',
    '#description' => t('A description of this type.'),
  );

  // The fields in the following fieldset allow us to configure the user journey
  // per reward type.
  $form['user_journey'] = array(
    '#type' => 'fieldset',
    '#title' => 'User Journey',
    '#group' => 'additional_settings',
  );

  // Two fields for specifying where the user should be redirected after
  // submitting the sign-up form for rewards of this type. One field for
  // anonymous users, one for authenticated users.
  foreach (array('anonymous', 'authenticated') as $role) {
    $variable_name = "gk_rewards_signup_redirect_path_{$reward_type->type}_$role";

    $form['user_journey'][$variable_name] = array(
      '#title' => t('!role sign-up redirect path', array(
        '!role' => ucfirst($role),
      )),
      '#type' => 'textfield',
      '#default_value' => variable_get($variable_name, ''),
      '#description' => t('Where should !role users be redirected after submitting the sign-up form?', array(
        '!role' => $role,
      )),
    );
  }

  // A field for specifying where the user should be redirected after they
  // confirm their details by clicking the link in the email.
  $variable_name = "gk_rewards_confirm_details_redirect_path_{$reward_type->type}";

  $form['user_journey'][$variable_name] = array(
    '#title' => 'Confirm details redirect path',
    '#type' => 'textfield',
    '#default_value' => variable_get($variable_name, ''),
    '#description' => 'Where should the user be redirected after confirming their details?',
  );

  // A field for specifying where the user should be redirected after they
  // submit the create account form.
  $variable_name = "gk_rewards_create_account_redirect_path_{$reward_type->type}";

  $form['user_journey'][$variable_name] = array(
    '#title' => 'Create account redirect path',
    '#type' => 'textfield',
    '#default_value' => variable_get($variable_name, ''),
    '#description' => 'Where should the user be redirected after submitting the create account form?',
  );

  // A field for specifying whether or not the user details form should be pre-
  // populated for logged in users.
  $form['user_details'] = array(
    '#type' => 'fieldset',
    '#title' => 'User Details',
    '#group' => 'additional_settings',
  );

  $variable_name = "gk_rewards_prepopulate_user_details_form_{$reward_type->type}";

  $form['user_details'][$variable_name] = array(
    '#title' => 'Pre-populate user details',
    '#type' => 'checkbox',
    '#default_value' => variable_get($variable_name, 1),
    '#description' => 'Pre-populate fields on the user details form for logged in users.',
  );

  // Vertical tabs.
  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 900,
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the reward_type entity form.
 */
function reward_type_form_submit(&$form, &$form_state) {
  $reward_type = entity_ui_form_submit_build_entity($form, $form_state);
  entity_save('reward_type', $reward_type);

  // Save the user redirect variables.
  foreach (array('anonymous', 'authenticated') as $user_status) {
    $variable_name = "gk_rewards_signup_redirect_path_{$reward_type->type}_$user_status";
    variable_set($variable_name, $form_state['values'][$variable_name]);
  }

  $variable_name = "gk_rewards_confirm_details_redirect_path_{$reward_type->type}";
  variable_set($variable_name, $form_state['values'][$variable_name]);

  $variable_name = "gk_rewards_create_account_redirect_path_{$reward_type->type}";
  variable_set($variable_name, $form_state['values'][$variable_name]);

  $variable_name = "gk_rewards_prepopulate_user_details_form_{$reward_type->type}";
  variable_set($variable_name, $form_state['values'][$variable_name]);

  // Redirect the user back to the list of reward_type entities.
  $form_state['redirect'] = 'admin/structure/reward-types';
}
