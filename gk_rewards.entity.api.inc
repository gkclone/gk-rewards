<?php

/**
 * @file
 * API functions for the reward(_type) entities.
 */

/**
 * Entity: reward ==============================================================
 */

/**
 * Entity access callback: reward
 */
function reward_access($op, $reward, $account = NULL, $entity_type = NULL) {
  if (!isset($account)) {
    $account = $GLOBALS['user'];
  }

  static $administer_rewards = array(
    'create' => 1, 'update' => 1, 'edit' => 1, 'delete' => 1,
  );
  if (isset($administer_rewards[$op])) {
    return user_access('administer rewards', $account);
  }

  if ($op == 'view') {
    return user_access('access content', $account);
  }
  elseif ($op == 'fulfil') {
    // Check the given account has received the given reward at some point.
    $query =
      db_select('gk_rewards_fulfilments', 'rf')
        ->condition('uid', $account->uid)
        ->condition('rid', $reward->rid)
        ->countQuery();

    return $query->execute()->fetchField() > 0;
  }
}

/**
 * Entity view callback: reward
 */
function reward_view($reward, $view_mode = 'full', $langcode = NULL, $page = NULL) {
  return entity_view('reward', array($reward), $view_mode, $langcode, $page);
}

/**
 * Entity load callback: reward
 */
function reward_load($tid, $reset = FALSE) {
  $rewards = reward_load_multiple(array($tid), array(), $reset);
  return reset($rewards);
}

/**
 * Entity load multiple callback: reward
 */
function reward_load_multiple($tids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('reward', $tids, $conditions, $reset);
}

/**
 * Entity: reward_type =========================================================
 */

/**
 * List of reward_type entities.
 */
function gk_rewards_type_list($type_name = NULL) {
  $types = entity_load_multiple_by_name('reward_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Entity access callback (used by admin UI): reward_type
 */
function reward_type_access($op, $entity = NULL, $account = NULL) {
  return user_access('administer reward types', $account);
}

/**
 * Entity load callback: reward_type
 */
function reward_type_load($reward_type) {
  return gk_rewards_type_list($reward_type);
}
