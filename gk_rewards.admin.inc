<?php

/**
 * A form for exporting user details to a CSV file.
 */
function gk_rewards_user_details_csv_export_form($form, &$form_state) {
  $count = db_select('gk_rewards_user_details')
    ->countQuery()->execute()->fetchField();

  $form['count'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('There are currently !count user details records in total.', array(
      '!count' => $count,
    )) . '</p>',
  );

  $form['date_from'] = array(
    '#type' => 'date_popup',
    '#date_format' => 'Y-m-d',
    '#title' => 'From',
  );

  $form['date_to'] = array(
    '#type' => 'date_popup',
    '#date_format' => 'Y-m-d',
    '#title' => 'To',
  );

  if ($GLOBALS['user']->uid == 1 && module_exists('devel')) {
    $form['debug'] = array(
      '#type' => 'checkbox',
      '#title' => 'Debug',
      '#default_value' => 1,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Export',
  );

  return $form;
}

/**
 * Submit handler for the user details export form.
 */
function gk_rewards_user_details_csv_export_form_submit($form, &$form_state) {
  // Debug mode?
  $debug = !empty($form_state['values']['debug']);

  // Attempt to open the PHP output stream.
  if (!$debug && !$handle = fopen('php://output', 'w')) {
    drupal_set_message('There was a problem and the CSV file could be generated.', 'error');
    return;
  }

  // With hundreds of thousands of records this could take a while...
  set_time_limit(0);

  $filename = 'user-details--' . date('d/m/Y--H:i:s') . '.csv';

  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Content-Type: text/csv; charset=utf-8; encoding=utf-8');
  header('Content-Disposition: attachment; filename="' . $filename . '"');

  // Build the headers.
  $header_info = gk_rewards_user_details_csv_export_header_info();
  $headers = array();

  foreach ($header_info as $key => $info) {
    $headers[$key] = $info['title'];
  }

  // Start spooling data. Start with the header row.
  !$debug && fputcsv($handle, $headers);

  // Get the user details records.
  $query = db_select('gk_rewards_user_details', 'ud')->fields('ud')
    ->addTag('gk_rewards_user_details_csv_export');

  if (!empty($form_state['values']['date_from'])) {
    $query->condition('created', strtotime($form_state['values']['date_from']), '>=');
  }

  if (!empty($form_state['values']['date_to'])) {
    $query->condition('created', strtotime($form_state['values']['date_to']), '<=');
  }

  $query_executed = $query->execute();

  // Process user details records individually.
  while ($user_details = $query_executed->fetch()) {
    $row = array();
    $user_details->data = unserialize($user_details->data);

    foreach ($header_info as $key => $info) {
      $data_value = isset($user_details->$key) ? $user_details->$key : NULL;

      if (!$value = $info['value callback']($data_value, $user_details)) {
        $value = '';
      }

      $row[] = $value;
    }

    if (!$debug) {
      fputcsv($handle, $row);
      ob_flush(); flush();
    }
    else {
      $rows[$user_details->udid] = array_combine(array_keys($headers), $row);
    }
  }

  if (!$debug) {
    fclose($handle);
    exit;
  }
  elseif (!empty($rows)) {
    dpm($rows);
  }
  else {
    drupal_set_message('There were no user details records to export.');
  }
}

/**
 * Return the header row for the user details CSV export file.
 */
function gk_rewards_user_details_csv_export_header_info() {
  $csv_headers = array(
    'mail' => array(
      'title' => 'Email',
      'value callback' => 'gk_rewards_user_details_csv_export_value_callback__verbatim',
      'weight' => 0,
    ),
    'title' => array(
      'title' => 'Title',
      'value callback' => 'gk_rewards_user_details_csv_export_value_callback__ucwords',
      'weight' => 10,
    ),
    'first_name' => array(
      'title' => 'Firstname',
      'value callback' => 'gk_rewards_user_details_csv_export_value_callback__ucwords',
      'weight' => 20,
    ),
    'last_name' => array(
      'title' => 'Lastname',
      'value callback' => 'gk_rewards_user_details_csv_export_value_callback__ucwords',
      'weight' => 30,
    ),
    'full_name' => array(
      'title' => 'Fullname',
      'value callback' => 'gk_rewards_user_details_csv_export_value_callback__full_name',
      'weight' => 40,
    ),
    'gender' => array(
      'title' => 'Gender',
      'value callback' => 'gk_rewards_user_details_csv_export_value_callback__gender',
      'weight' => 50,
    ),
    'postcode' => array(
      'title' => 'Postcode',
      'value callback' => 'gk_rewards_user_details_csv_export_value_callback__verbatim',
      'weight' => 60,
    ),
    'date_of_birth' => array(
      'title' => 'Dob',
      'value callback' => 'gk_rewards_user_details_csv_export_value_callback__verbatim',
      'weight' => 70,
    ),
    'comms_opt_in_brand' => array(
      'title' => 'OptInBrand',
      'value callback' => 'gk_rewards_user_details_csv_export_value_callback__boolean',
      'weight' => 80,
    ),
    'comms_opt_in_other' => array(
      'title' => 'OptInOther',
      'value callback' => 'gk_rewards_user_details_csv_export_value_callback__boolean',
      'weight' => 90,
    ),
    'created' => array(
      'title' => 'Created',
      'value callback' => 'gk_rewards_user_details_csv_export_value_callback__date',
      'weight' => 100,
    ),
    'changed' => array(
      'title' => 'Changed',
      'value callback' => 'gk_rewards_user_details_csv_export_value_callback__date',
      'weight' => 110,
    ),
    'data' => array(
      'title' => 'Data',
      'value callback' => 'gk_rewards_user_details_csv_export_value_callback__data',
      'weight' => 1000,
    ),
  );

  // Make it possible for modules to modify the headings.
  drupal_alter('gk_rewards_user_details_csv_export_headers', $csv_headers);
  uasort($csv_headers, 'drupal_sort_weight');

  return $csv_headers;
}

/**
 * User details CSV export value callback: Verbatim (as is)
 */
function gk_rewards_user_details_csv_export_value_callback__verbatim($data_value, $user_details) {
  return $data_value;
}

/**
 * User details CSV export value callback: Boolean (Y/N)
 */
function gk_rewards_user_details_csv_export_value_callback__boolean($data_value, $user_details) {
  static $yes_no = array(
    0 => 'N',
    1 => 'Y',
  );
  return $yes_no[$data_value];
}

/**
 * User details CSV export value callback: ucwords() wrapper
 */
function gk_rewards_user_details_csv_export_value_callback__ucwords($data_value, $user_details) {
  return ucwords($data_value);
}

/**
 * User details CSV export value callback: Gender
 */
function gk_rewards_user_details_csv_export_value_callback__gender($data_value, $user_details) {
  static $genders = array(
    'mr' => 'Male',
    'mrs' => 'Female',
    'ms' => 'Female',
    'miss' => 'Female',
  );
  if (isset($genders[$user_details->title])) {
    return $genders[$user_details->title];
  }
}

/**
 * User details CSV export value callback: Full name
 */
function gk_rewards_user_details_csv_export_value_callback__full_name($data_value, $user_details) {
  return ucwords(implode(' ', array($user_details->first_name, $user_details->last_name)));
}

/**
 * User details CSV export value callback: Date
 */
function gk_rewards_user_details_csv_export_value_callback__date($data_value, $user_details) {
  return date('jS M Y - H:i:s', $data_value);
}

/**
 * User details CSV export value callback: Data
 */
function gk_rewards_user_details_csv_export_value_callback__data($data_value, $user_details) {
  if (!empty($user_details->data)) {
    $values = array();

    foreach ($user_details->data as $key => $datum) {
      $values[] = $key . ': ' . print_r($datum, TRUE);
    }

    return implode("\n", $values);
  }
}
