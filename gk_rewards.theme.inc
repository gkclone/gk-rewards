<?php

/**
 * @file
 * Theming functions for the Rewards module.
 */

/**
 * Preprocess variables for reward.tpl.php
 */
function gk_rewards_preprocess_entity(&$variables) {
  switch ($variables['entity_type']) {
    case 'reward':
      $reward = $variables['reward'];

      // Attributes array.
      $variables['attributes_array']['id'] = 'reward-' . $reward->rid;

      $variables['attributes_array']['class'][] = 'Reward';
      $variables['attributes_array']['class'][] = 'Reward--' . minima_camelcase($reward->type);
      $variables['attributes_array']['class'][] = 'Reward--' . ($reward->public ? 'p' : 'notP') . 'ublic';
      $variables['attributes_array']['class'][] = 'Reward--' . ($reward->status ? 'p' : 'notP') . 'ublished';
    break;

    case 'tier':
      $tier = $variables['tier'];

      // If the entity is a reward_tier and the referrals module is enabled then
      // add classes to the output to show which tiers the user has reached.
      if ($tier->type == 'reward_tier' && !empty($tier->group) && module_exists('gk_referrals')) {
        if (gk_rewards_tier_group_user_referral_count($tier->group) >= $tier->threshold) {
          $variables['attributes_array']['class'][] = 'Tier--reached';
        }
        else {
          $variables['attributes_array']['class'][] = 'Tier--notReached';
        }
      }
    break;
  }
}

/**
 * Preprocess variables for the "confirm details" mail template.
 */
function template_preprocess_gk_rewards_confirm_details_mail(&$variables) {
  $variables['intro'] = t('Hello !first_name', array(
    '!first_name' => $variables['user_details']->first_name,
  ));

  $variables['summary'] = t('Please click the link below to validate your email address.');

  $variables['confirm_details_url'] = url('confirm-my-details', array(
    'absolute' => TRUE,
    'query' => array(
      'h' => $variables['user_details']->hash,
    ),
  ));

  $variables['confirm_details_text'] = t('Confirm your details');
}

/**
 * Preprocess variables for the "reward tier notify" mail template.
 */
function template_preprocess_gk_rewards_reward_tier_notify_mail(&$variables) {
  $account_wrapper = entity_metadata_wrapper('user', $variables['account']);

  $variables['intro'] = t('Hello !first_name,', array(
    '!first_name' => $account_wrapper->field_user_first_name->value(),
  ));

  $variables['summary'] = t('You are now eligible for rewards at !sitename. Login to see your rewards:', array(
    '!sitename' => variable_get('site_name'),
  ));

  $link_options = array('absolute' => TRUE);

  // If we can get a path to the rewards node then set the destination parameter
  // of our login link.
  if ($uri = gk_machine_name_lookup_uri('gk_rewards_rewards')) {
    $link_options['query']['destination'] = $uri['path'];
  }

  $variables['ctas'] = array(
    'login' => array(
      'url' => url('user', $link_options),
      'title' => t('Login'),
    ),
  );

  $variables['outro'] = t('Regards,<br />!site_name', array(
    '!site_name' => variable_get('site_name'),
  ));
}
