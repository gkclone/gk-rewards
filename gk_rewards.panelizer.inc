<?php
/**
 * @file
 * gk_rewards.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function gk_rewards_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:gk_rewards_login_or_create_account';
  $panelizer->title = 'Rewards: Login or create account';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '1_column';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '10763256-9e38-4faf-b031-105a06161572';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-26de0adc-ff55-482f-91f0-f4aea01edc30';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '26de0adc-ff55-482f-91f0-f4aea01edc30';
    $display->content['new-26de0adc-ff55-482f-91f0-f4aea01edc30'] = $pane;
    $display->panels['primary'][0] = 'new-26de0adc-ff55-482f-91f0-f4aea01edc30';
    $pane = new stdClass();
    $pane->pid = 'new-177299b2-4d8c-48c6-975a-a8c461665581';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '177299b2-4d8c-48c6-975a-a8c461665581';
    $display->content['new-177299b2-4d8c-48c6-975a-a8c461665581'] = $pane;
    $display->panels['primary'][1] = 'new-177299b2-4d8c-48c6-975a-a8c461665581';
    $pane = new stdClass();
    $pane->pid = 'new-e01673a6-3622-4acb-9271-e8e9771b15e2';
    $pane->panel = 'primary';
    $pane->type = 'block';
    $pane->subtype = 'gk_rewards-gk_rewards_login_create_account';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'e01673a6-3622-4acb-9271-e8e9771b15e2';
    $display->content['new-e01673a6-3622-4acb-9271-e8e9771b15e2'] = $pane;
    $display->panels['primary'][2] = 'new-e01673a6-3622-4acb-9271-e8e9771b15e2';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:gk_rewards_login_or_create_account'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:gk_rewards_rewards';
  $panelizer->title = 'Rewards: Rewards';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '2_columns_25_75';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
      'secondary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '6f7b7ebc-f151-40ed-92e3-945ae1aaa271';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-9f7eb01d-b2e8-4c7d-9375-a549fa08dd8e';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9f7eb01d-b2e8-4c7d-9375-a549fa08dd8e';
    $display->content['new-9f7eb01d-b2e8-4c7d-9375-a549fa08dd8e'] = $pane;
    $display->panels['primary'][0] = 'new-9f7eb01d-b2e8-4c7d-9375-a549fa08dd8e';
    $pane = new stdClass();
    $pane->pid = 'new-9c4944f7-13cd-4de3-84b1-f7f70c38238e';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '9c4944f7-13cd-4de3-84b1-f7f70c38238e';
    $display->content['new-9c4944f7-13cd-4de3-84b1-f7f70c38238e'] = $pane;
    $display->panels['primary'][1] = 'new-9c4944f7-13cd-4de3-84b1-f7f70c38238e';
    $pane = new stdClass();
    $pane->pid = 'new-1abc364b-275e-4b90-b33a-39e2ec920307';
    $pane->panel = 'primary';
    $pane->type = 'block';
    $pane->subtype = 'gk_rewards-gk_rewards_user_rewards';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '1abc364b-275e-4b90-b33a-39e2ec920307';
    $display->content['new-1abc364b-275e-4b90-b33a-39e2ec920307'] = $pane;
    $display->panels['primary'][2] = 'new-1abc364b-275e-4b90-b33a-39e2ec920307';
    $pane = new stdClass();
    $pane->pid = 'new-d8887ebd-4fb0-421c-9002-9aab119a24fd';
    $pane->panel = 'secondary';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-members-user-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd8887ebd-4fb0-421c-9002-9aab119a24fd';
    $display->content['new-d8887ebd-4fb0-421c-9002-9aab119a24fd'] = $pane;
    $display->panels['secondary'][0] = 'new-d8887ebd-4fb0-421c-9002-9aab119a24fd';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:gk_rewards_rewards'] = $panelizer;

  return $export;
}
